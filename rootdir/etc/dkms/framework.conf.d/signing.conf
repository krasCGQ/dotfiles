#!/hint/bash
# shellcheck disable=SC2034
#
# SPDX-License-Identifier: Unlicense
#

# Temporary variables to determine if kernel headers provide signing key
_mok_path=$(
    # Parse available kernels from /usr/lib/modules; this is a messy stuff to be honest
    find /usr/lib/modules -mindepth 1 -maxdepth 1 | while IFS= read -r path; do
        # We only need the kernel version itself
        __kernver=$(echo "$path" | cut -d/ -f5)
        # Ensure we are picking correct signing key for building
        [[ $* =~ $__kernver && -r $path/build/certs/signing_key.pem ]] && {
            echo "$path/build/certs/signing_key"
            break
        }
    done
    unset __kernver
)
# Sorry DKMS, but kernel source generates with these extensions
[[ -n $_mok_path ]] && {
    _mok_key_ext=pem
    _mok_cert_ext=x509
}

# Re-use kernel-provided key if any, otherwise fallback to auto-generated MOK key
mok_signing_key="${_mok_path:-/var/lib/dkms/mok}.${_mok_key_ext:-key}"
mok_certificate="${_mok_path:-/var/lib/dkms/mok}.${_mok_cert_ext:-pub}"

# We don't need these anymore, unset it
unset _mok_path _mok_key_ext _mok_cert_ext
